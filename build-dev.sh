docker-compose down
docker-compose stop
docker-compose pull
docker-compose build --no-cache
docker-compose up -d --force-recreate --remove-orphans
