git fetch --all
git reset --hard origin/master
kill $(sudo lsof -i tcp:8080 | awk '{print $2}' | sed '1d')
mvn clean spring-boot:run &