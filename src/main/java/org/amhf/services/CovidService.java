package org.amhf.services;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.amhf.models.Country;
import org.amhf.models.DailyData;
import org.amhf.repositories.CovidRepository;
import org.amhf.repositories.DailyDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CovidService {

  @Autowired
  private CovidRepository covidRepository;

  @Autowired
  private DailyDataRepository dailyDataRepository;

  public void update(List<Country> dataset) {
    Map<String, DailyData> map = new HashMap<>();
    dataset.forEach(e -> {
      for (DailyData dailyData : e.getDailyData()) {
        dailyData
            .setActive(dailyData.getConfirmed() - dailyData.getDeaths() - dailyData.getRecovered());
        if (map.containsKey(dailyData.getDate())) {
          map.get(dailyData.getDate()).add(dailyData);
        } else {
          map.put(dailyData.getDate(), dailyData.clone());
        }
      }
    });
    List<DailyData> values = new ArrayList<>(map.values());
    values.sort(Comparator.comparing(DailyData::getDate).reversed());
    covidRepository.saveAll(dataset);
    dailyDataRepository.saveAll(values);
  }

  public Optional<DailyData> getLatestData(String country) {
    return covidRepository.findLatestDataByCountry(country);
  }

  public Optional<Country> getDailyData(String country, String date) {
    return covidRepository.findOneByCountryAndDailyData_Date(country, date);
  }

  public Iterable<Country> getDailyDataForAll(String date) {
    return covidRepository.findByDailyData_Date(date);
  }

  public Iterable<Country> getDataset() {
    return covidRepository.findAll();
  }

  public Optional<Country> getCountryData(String country) {
    return covidRepository.findById(country);
  }

  public Iterable<DailyData> getDailyTotals() {
    return dailyDataRepository.findAllByOrderByDateAsc();
  }

  public Iterable<Country> getTopNByProperty(int n, String property) {
    return covidRepository.findTopNByProperty(n, property);
  }
}
