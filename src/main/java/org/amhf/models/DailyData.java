package org.amhf.models;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.springframework.data.annotation.Id;

public class DailyData {

  private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-M-d");
  private int confirmed;
  private int active;
  private int deaths;
  private int recovered;
  @Id
  private String date;

  public DailyData() {
  }

  public DailyData(int confirmed, int deaths, int recovered, String date) {
    this.confirmed = confirmed;
    this.active = confirmed - deaths - recovered;
    this.deaths = deaths;
    this.recovered = recovered;
    this.date = date;
  }

  public int getConfirmed() {
    return confirmed;
  }

  public void setConfirmed(int confirmed) {
    this.confirmed = confirmed;
  }

  public int getActive() {
    return active;
  }

  public void setActive(int active) {
    this.active = active;
  }

  public int getDeaths() {
    return deaths;
  }

  public void setDeaths(int deaths) {
    this.deaths = deaths;
  }

  public int getRecovered() {
    return recovered;
  }

  public void setRecovered(int recovered) {
    this.recovered = recovered;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = LocalDate.parse(date, DATE_FORMATTER).format(DateTimeFormatter.ISO_LOCAL_DATE);
  }

  public void add(DailyData a) {
    this.active += a.active;
    this.recovered += a.recovered;
    this.confirmed += a.confirmed;
    this.deaths += a.deaths;
  }

  public DailyData clone() {
    return new DailyData(getConfirmed(), getDeaths(), getRecovered(), getDate());
  }
}
