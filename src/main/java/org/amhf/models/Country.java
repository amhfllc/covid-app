package org.amhf.models;

import java.util.Comparator;
import java.util.List;
import org.springframework.data.annotation.Id;

public class Country {

  @Id
  private String country;
  private DailyData latestData;
  private List<DailyData> dailyData;

  public Country(String country, List<DailyData> dailyData) {
    this.country = country;
    dailyData.sort(Comparator.comparing(DailyData::getDate).reversed());
    this.dailyData = dailyData;
    this.latestData = dailyData.size() > 0 ? dailyData.get(0) : null;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public List<DailyData> getDailyData() {
    return dailyData;
  }

  public void setDailyData(List<DailyData> dailyData) {
    this.dailyData = dailyData;
  }

  public DailyData getLatestData() {
    return latestData;
  }

  public void setLatestData(DailyData latestData) {
    this.latestData = latestData;
  }
}
