package org.amhf.repositories;

import org.amhf.models.Country;

public interface CovidRepositoryCustom {

  Iterable<Country> findTopNByProperty(int n, String property);
}
