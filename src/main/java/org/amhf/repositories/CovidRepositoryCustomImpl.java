package org.amhf.repositories;

import org.amhf.models.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

public class CovidRepositoryCustomImpl implements CovidRepositoryCustom {

  @Autowired
  MongoTemplate mongoTemplate;

  public Iterable<Country> findTopNByProperty(int n, String property) {
    Query query = new Query();
    query.with(Sort.by("latestData." + property).descending())
        .limit(n);
    return mongoTemplate.find(query, Country.class);
  }
}
