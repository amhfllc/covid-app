package org.amhf.repositories;

import org.amhf.models.DailyData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DailyDataRepository extends CrudRepository<DailyData, String> {

  Iterable<DailyData> findAllByOrderByDateAsc();
}
