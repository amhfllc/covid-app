package org.amhf.repositories;

import java.util.Optional;
import org.amhf.models.Country;
import org.amhf.models.DailyData;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CovidRepository extends CrudRepository<Country, String>, CovidRepositoryCustom {

  @Query(value = "{ '_id': ?0 }", fields = "{ 'latestData': 1 }")
  Optional<DailyData> findLatestDataByCountry(String country);

  @Query(value = "{ '_id': ?0, 'dailyData._id': ?1 }", fields = "{ 'dailyData.$': 1 }")
  Optional<Country> findOneByCountryAndDailyData_Date(String country, String date);

  @Query(value = "{ 'dailyData._id': ?0 }", fields = "{ 'dailyData.$': 1 }")
  Iterable<Country> findByDailyData_Date(String date);
}
