package org.amhf.controllers;

import java.io.File;
import java.io.IOException;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class AdminController {

  private static Logger logger = LoggerFactory.getLogger(AdminController.class);

  @Value("${GITLAB_PUSH_HOOK_SECRET_KEY}")
  private String gitlabSecretKey;

  @PostMapping("/git-push-webhook/file/{fileName}")
  public ResponseEntity<String> gitPushWebhook(
      @RequestHeader(value = "X-Gitlab-Token") String secretKey,
      @PathVariable("fileName") String fileName) {
    if (!gitlabSecretKey.equals(secretKey)) {
      logger.warn(
          "Unknown entity accessing this endpoint /api/admin/git-push-webhook/file/{} with secret key: {}",
          fileName, secretKey);
      return ResponseEntity.status(Response.SC_FORBIDDEN)
          .body("You do not have sufficient privileges to call this API");
    }
    try {

      ProcessBuilder processBuilder = new ProcessBuilder("/bin/sh",
          new File(fileName).getAbsolutePath());
      processBuilder.start();
    } catch (IOException e) {
      logger.error("Unable to execute the latest build. ", e);
    }
    return ResponseEntity.noContent().build();
  }
}
