package org.amhf.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.amhf.models.Country;
import org.amhf.models.DailyData;
import org.amhf.services.CovidService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/covid")
public class CovidController {

  public static final Logger logger = LoggerFactory.getLogger(CovidController.class);
  private static final String SOURCE_URL = "https://pomber.github.io/covid19/timeseries.json";
  private static final List<String> properties = List
      .of("active", "recovered", "deaths", "confirmed");
  @Autowired
  private CovidService covidService;

  @GetMapping("/updater")
  public void updater() {
    RestTemplate client = new RestTemplate();
    String response = client.getForObject(SOURCE_URL, String.class);
    try {
      covidService.update(mapJsonToModel(response));
    } catch (IOException e) {
      logger.error("Unable to map json response to internal models", e);
    }
  }

  @GetMapping("/data")
  public ResponseEntity data(@RequestParam(name = "country", required = false) String country,
      @RequestParam(name = "date", required = false) String date) {
    if (country == null && date == null) {
      return ResponseEntity.ok(covidService.getDataset());
    } else if (country != null && date == null) {
      return ResponseEntity.ok(covidService.getCountryData(country));
    } else if (country == null) {
      return ResponseEntity.ok(covidService.getDailyDataForAll(date));
    } else {
      return ResponseEntity.ok(covidService.getDailyData(country, date));
    }
  }

  @GetMapping("/topN")
  public ResponseEntity data(@RequestParam(name = "n", required = false, defaultValue = "10") int n,
      @RequestParam(name = "property", required = false, defaultValue = "confirmed") String property) {
    if (!properties.contains(property)) {
      return ResponseEntity.badRequest().build();
    }
    return ResponseEntity.ok(covidService.getTopNByProperty(n, property));
  }

  @GetMapping("/totals")
  public ResponseEntity totals() {
    return ResponseEntity.ok(covidService.getDailyTotals());
  }

  private List<Country> mapJsonToModel(String json) throws IOException {
    Map<String, List<DailyData>> map = new ObjectMapper()
        .readValue(json, new TypeReference<HashMap<String, List<DailyData>>>() {
        });
    return map.entrySet().stream()
        .map(e -> new Country(e.getKey(), e.getValue()))
        .collect(Collectors.toUnmodifiableList());
  }
}
