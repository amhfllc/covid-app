package org.amhf.config;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class CORSConfigurer {

  @Value("${CORS_ORIGINS_TO_ALLOW}")
  private String[] corsOrigin;

  @Value("${CORS_PATH_PATTERNS}")
  private String[] pathPatterns;

  @Bean
  public WebMvcConfigurer corsConfigurer() {
    return new WebMvcConfigurer() {
      @Override
      public void addCorsMappings(CorsRegistry registry) {
        Arrays.stream(pathPatterns).map(registry::addMapping).forEach(
            registration -> registration.allowedOrigins(corsOrigin));
      }
    };
  }
}
